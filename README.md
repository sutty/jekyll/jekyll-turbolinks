# Turbolinks for Jekyll

[Turbolinks](https://github.com/turbolinks/turbolinks) is a Javascript
library for accelerating web navigation by preventing the browser to
reload the full page, thus making it reload Javascript and CSS.

This plugin enables Turbolinks in all generated pages and adds Liquid
tags to insert `turbolinks.js` into your template in case you want to
include in your Javascript.

## How to use it

### Setup

Add `jekyll-turbolinks` to your `Gemfile`:

```ruby
group :jekyll_plugins do
  gem 'jekyll-turbolinks'
end
```

And add it to your `_config.yml` file:

```yaml
plugins:
  - jekyll-turbolinks

# Optional
turbolinks:
  enabled: true # Set to false if you want to use the tag
  url: turbolinks.js
```

You can also control if you want a page to have turbolinks disabled by
adding this to its front matter:

```yaml
---
turbolinks: false
---
```

### Automatic

That's it!  Turbolinks will be loaded and enabled by default.

Keep in mind that if you're not using [relative urls](https://0xacab.org/sutty/jekyll/jekyll-relative-urls) you must set the `turbolinks` path to an absolute path, like so:

```yaml
turbolinks:
  url: /turbolinks.js
```

### Manual

To add it manually call the `turbolinks` Liquid tag to insert the actual
contents of `turbolinks.js` into your Javascript file.

```liquid
---
# The triple dashes are needed for Jekyll to process this file as a
# Liquid template
---

{% turbolinks %}

// ... rest of your Javascript
```

And call it from your `_layouts/default.html` file:

```html
<head>
  <script type="text/javascript" src="path/to/file.js"></script>
</head>
```


## License

Copyright (c) 2019 Sutty <hi@sutty.nl>

jekyll-turbolinks is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

jekyll-turbolinks is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along
with jekyll-turbolinks.  If not, see <https://www.gnu.org/licenses/>.
