# frozen_string_literal: true

require 'turbolinks/source'
require_relative 'jekyll/tags/turbolinks'

module Jekyll
  # Access Turbolinks source
  class Turbolinks
    class << self
      def path
        File.join ::Turbolinks::Source.asset_path, 'turbolinks.js'
      end

      def config(site)
        {
          'enabled' => true,
          'url' => 'turbolinks.js'
        }.merge(site.config.dig('turbolinks') || {})
      end

      def dest(site, config)
        File.join site.source, config['url']
      end
    end
  end
end

# Copies the turbolinks source into the site and adds it to every pages
# <head>.
Jekyll::Hooks.register :site, :post_render do |site|
  config = Jekyll::Turbolinks.config(site)

  next unless config['enabled']

  dest = Jekyll::Turbolinks.dest site, config
  rel_url = config['url'].gsub(%r{\A/}, '')

  # Copy the turbolinks file to source
  FileUtils.mkdir_p File.dirname(dest)
  FileUtils.cp Jekyll::Turbolinks.path, dest

  # XXX: We can't copy to destination and use keep_files because
  # jekyll-gzip will ignore it
  site.static_files << Jekyll::StaticFile.new(
    site,
    site.source,
    File.dirname(rel_url),
    File.basename(rel_url)
  )

  # Process each file and add the script
  site.each_site_file do |file|
    next unless file.respond_to? :output
    next unless file.data.fetch('turbolinks', true)
    next if file.output.nil?

    file.output
        .gsub! %r{</head>},
               "<script defer src=\"#{config['url']}\"></script></head>"
  end
end

# Cleanup
# TODO: This doesn't remove the directory if turbolinks.js was put
# somewhere else
Jekyll::Hooks.register :site, :post_write do |site|
  config = Jekyll::Turbolinks.config(site)

  next unless config['enabled']

  FileUtils.rm_f Jekyll::Turbolinks.dest(site, config)
end
