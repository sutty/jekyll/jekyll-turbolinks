# frozen_string_literal: true

module Jekyll
  module Tags
    # Crea un tag de Liquid que trae el contenido de turbolinks.js
    class Turbolinks < Liquid::Tag
      def render(_context)
        File.read(Jekyll::Turbolinks.path)
      end
    end
  end
end

# Registrar el tag
Liquid::Template.register_tag('turbolinks', Jekyll::Tags::Turbolinks)
